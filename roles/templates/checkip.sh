#!/bin/bash

while true; do
  output=$(curl -s --connect-timeout 5 --max-time 10 --retry 5 --retry-delay 0 --retry-max-time 50 http://checkip.dyndns.org)
  echo "$(date +%s) | ${output}" >> /opt/checkip/output.txt
  sleep 60s
done

